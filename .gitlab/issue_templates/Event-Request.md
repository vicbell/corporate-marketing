<!-- This section is to be used by anyone internally requesting GitLab sponsor a corporate event. Not sure what qualifies, please visit see the [events decision tree](https://docs.google.com/spreadsheets/d/1aWsmsksPfOlX1t6TeqPkh5EQXergt7qjHAjGTxU27as/edit?usp=drive_web&ouid=110206726155880220171). Please fill all the necessary information out. If you don’t need a section, feel empowered to delete it from the issue. Remember to always add Related Issues and Epics after you've created your issue so folks have context on what other issues connect to this work. Keep being awesome! 

* title the issue: `"Name of event", Location, Date of Event` (add them remove this line)
* Make the due date the day the event starts
* `Assign the Quarter the work will happen in, and Region tags to the issue`
-->

## :bulb: Background/purpose/ Why should we do this?

<!-- Give an sentence overview of why this issue exists. Justification for wanting to sponsor the event and any background you have (please be as detailed as possible). Please ntoe we cannot evaluate any events without context.   -->

## :map: Details and reach

<!-- Include details on why you're completing this work, how it impacts the business, and the potential reach if applicable. Include key dates as well.  -->
* **Official Event Name:** 
* **Date:**
* **Location:** 
* **Event Website:**
* **Expected Number of Attendees at the event:**
* **GitLab Staffing Needs:**
* **Dress Code/ Attire:**
* **GitLab Hosted Landing page (if applicable):**
* **Speakers (if applicable):**
* **SFDC Campaign:**
* **Attendee List/ Audience Demographics:**
* **If we went last year, link to SF.com campaign:** 

### :dart: Goals, audience and key messages

<!-- Add context on the goals for this work. Add key messages if applicable. This helps those who may be unfamiliar with your functional group have a better understanding of the work.  -->* **Sponsorship (if applicable):** outline level, benefits and cost breakdown.
* **Event Goals:**
  * Goals for # of leads: 
  * Goals for pipeline created $$$: 
  * Number of meetings at event: 
  * registration goals/ attendance goals: 

`**Once this section is complete you can go into contracting. To beging budget approval and contract review start an issue in the [procurement project](https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/-/blob/master/.gitlab/issue_templates/vendor_contracts_marketing_events.md) using the "vendor_contracts_marketing_events" tenmplate. Copying/pasting the 'campaign tag' from above. Be sure to include any concessions you have negotiated or discounts we are getting in step to of the template.**

---
### :notebook_with_decorative_cover: Event brief

<!-- outline details of our involvement/ commitment. Be as detailed as possible -->
---

## :star: Finance Process for DRI

<!-- If this issue is connected to a related finance issue, link the above subhead for easy discover. If not, you can remove. -->
* [Budget Doc](https://docs.google.com/spreadsheets/d/1AvL2Rkwnr7pYtkMYXualfVVAJc5Irzo8Abiw9cybwdY/edit) - Copy template and start new budget for each event. Add to finance issue when you start it. `Budget Doc Naming Convention: "ISOdate_CampaignShortName_Budget". It is the responsiobilty of the DRI to keep the budget doc up-to-date and to notify their manager as soon as or if you think you will go over budget` 
* `Finance Tag(s)` - create the 'campaign tag' (using proper ISOdate_name, noting 37 character limit) then add into the associated budget line item. Each campaign that needs tracking should get it's own tag. 
**Budgeted costs (sponsorship + auxiliary cost (AV, booth, swag, travel, printed materials...)):** `DRI to fill in`
* [ ]  Event [evaluated and aligned to regional and company goals](https://about.gitlab.com/handbook/marketing/events/#how-we-evaluate-and-build-potential-events)
* [ ]  Spend added to [Non Headcount Budget Sheet](https://docs.google.com/spreadsheets/d/1WVWZjSF6f5jAFqHO4hXcV8mN975ITT4eXScSn0F_FU8/edit#gid=0)
* [ ]  Finance issue created in [Issues · GitLab.com / Finance · GitLab](https://gitlab.com/gitlab-com/finance/issues) + add budget sheet to finance issue 
* [ ]  **Once contract signed start an EVENT EPIC**. Use the Epic template found in [here](https://about.gitlab.com/handbook/marketing/events/#mpm-steps-to-set-up-event-epic). Asociate this issue and any others relating to the project to the epic. Close this issue and make the EPIC the SSOT for project information goigng forrward.

* [ ]  Invoice received. [Instructions on procure to pay](https://about.gitlab.com/handbook/finance/accounting/#procure-to-pay). 
* [ ]  Invoice paid


/assign @emily

/label ~Events ~"Field Marketing" ~"MktgOps - FYI" ~"MPM - Radar" ~"Marketing Programs" ~"Marketing Campaign" ~"Corporate Event" ~"mktg-status::plan" ~"META"