# Request Details

## :ballot_box_with_check: Type of Task/Request
> To help us track our [DE Budgets](https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/index.html#developer-evangelism-request-budgets) please assign a label that identifies the requesting team and a weight correlating with this issue's [budget score](https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/index.html#scoring-requests) to allow us to track each team's budget consumption.
> For tracking purposes, please add the respective `DE-Task` label corresponding to the type of request you choose.
> More details at https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/#issue-tracker 


**Team/Personal Task (DE Team Only):**
> Developer Evangelism Team's Personal or team tasks that needs to be tracked, please add/change the `DE-Request-Type::Internal` label below.

* [ ] Content Creation
* [ ] CFP Submission
* [ ] Event Attendance (Meetup/Conference/etc.)
* [ ] Speaking Engagement
* [ ] LiveStream / Webcast
* [ ] Community Contribution


**External Requests:**
> Requests from other team members within GitLab, please add the `DE-Request-Type::External` label (default below).

* [ ]  Blog / Content Creation
* [ ]  Speaking Engagement (in-person)
* [ ]  Event Coverage (in-person)
* [ ]  Webcast
* [ ]  Media Coverage (interview, etc.)
* [ ]  CFP Submission Review
* [ ]  Content Review Request
* [ ]  Speaking Coaching
* [ ]  Other

## :page_with_curl: Request 

* Request description: `A Great Event`
* Requested by: `@you`
* Due Date: `2021-01-01`

If it's an event, please provide the following details, remove this section otherwise:

* Event URL: `https://example.com`
* Location: `Virtual/Somewhere, Worldwide`
* Event Dates: `2021-01-01`

## :building_construction: Request Triage Information
> This information will help the Developer Evangelism team triage the request and understand its impact when weighed against other requests in flight.

* Audience Description: `Who is this for?  What's the intended audience?`
* Expected Audience Size (live): `500`
* Expected Audience Size (post-creation online audience): `1200`
* Is this a new/nascent audience for GitLab (e.g. .NET developers)? `Yes/No`
* If this is a speaking session, will it be recorded and available for promotion? `Yes/No`
* [ ] Label indicating requestor's team and issue weight have been added, learn more [here](https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/index.html#scoring-requests)

> If the need is within the next week, please also slack the team in the [`#developer-evangelism`](https://app.slack.com/client/T02592416/CMELFQS4B) channel.

## :speaking_head: Request Details 
> Add as many details as possible about the need.

# Post Request Completion

## Request Completion Details

* Date of Execution/Completion: `<Date>`
* Link to resource: `<Video, Link, Doc, etc.>`
* Other Relevant Details: 

## Metrics

*Please provide any available metrics or accessible link to metrics*

## DE CheckList

* [ ] Added to [team calendar](https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/#-team-calendar)
* [ ] Added to Pathfactory
* [ ] Metrics Added to DE Metrics Sheet
* [ ] Repurposed

/label ~"dev-evangelism" ~"DE-Request::New" ~"DE-Request-Type::External" ~"DE-Content::new-request"

/assign @abuango
