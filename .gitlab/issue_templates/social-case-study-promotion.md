### Request for organic (non-paid) Case Study Promotion on Social Media
<!-- Requester: Please name this issue: `Organic Social Case Study: name of customer` --> 

<!-- Requester please fill in all sections above the solid line. Some details may not be applicable. --> 
#### Please Acknowledge
* [ ] Customer has approved sharing on social and we've obtained their social media info 

### 📬 STEP 1: For Requester 
**1.  Who is the customer and what is the core story we're looking to tell?**

`add details here`

**2.  Pertinent Dates**
* [ ] Add the intended date the case study goes live as the due date in the right column ➡️

**3.  Is there a landing page?**

`once the case study URL is available, please add it here. Note, that it doesn't need to be live, just selected.`

**4. Customer Social Media info**

`please link or add the customer social media info they've provided here`

**5.  Who is your intended audience? Note, that we cannot use targeting parameters on organic social. This helps us write copy.**

`insert intended audiences here`

**6. Assets and logos?**  <!-- links with cards, images, or videos -->
* [ ]  Logos are approved to use and available here: `insert link to related issue or repo for assets`
* [ ]  We won't get logos to use
* [ ]  I require custom images for social
* [ ]  I'm not sure what I require (the social team will review with you in the comments below)

**7. Are you considering or will be adding paid social advertising for this campaign?** <!-- Note that paid social advertising is managed by the digital marketing programs team and will require a separate issue -->
* [ ]  Yes, I'm requesting paid social advertising `add link to paid social media issue`
* [ ]  No, I am not requesting paid social advertising
* [ ]  I'm not sure (the social team will help you determine the right path)

**8.  Anything else we should know or could be helpful to know?**

`Add additional thoughts here. Subjective things are helpful. This is a new initiative, we've never promoted this topic, our audience is loosely defined, etc.`

**9. Please link all related issues in the *related issues section below* 👇**

-----

### 📝 STEP 2: Social Team: To-Do's
* [ ]  Add to an appropriate milestone given to time of publishing
* [ ]  Asset Review: if asset links were provided, please review to be appropriate for social. If existing assets are not appropriate or if the requester asked for custom assets, please use Canva to accomplish. If this is tied to a larger campaign, please open a design issue, tag requester, and link as a related issue here.

### 🗓 STEP 3: Social Team: Scheduled posts
* [ ]  All posts that can be added to Sprout are scheduled
* [ ]  Tag the requester of the issue and attach a view of the scheduled posts. 

### 📭 Once the posts available to add to Sprout are scheduled you may close this issue. Requester, if you have feedback or required updates, please reopen the issue and tag the social team member who handled your request.

/label ~"Social Media" ~"Corp Comms"~"Corporate Marketing" ~"mktg-status::plan"

/assign @social

/confidential

/milestone %"Organic Social: Triage"