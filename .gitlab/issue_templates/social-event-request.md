#### Request for corporate marketing-sponsored event related organic (non-paid) social promotion for `add event name`
<!-- **** This issue template is for the corporate events marketing team to use in aiding with a social campaign. If the event is not attached to corporate marketing, we won't be able to support on our brand social channels. -->

## 📬 STEP 1: For Requester <!-- Requester please fill in all **[sections]** above the solid line for STEP 1. Please do not open the issue until you're ready to answer the following questions. If the social team still needs more info, they'll ask in the comments. Note that these questions are critical for understanding your request. -->
#### Event Details
1. Pertinent Event date(s):<!-- Please include dates like ticket sale launches, Speaker dates, actual event date(s), etc. -->

**[add date(s) here]**

2. 🔗 Add appropriate link(s) for user journey; Consider required creative
- [ ] None, this is a social only/first ask
- [ ] Yes, it's a GitLab owned link; `add link here`
- [ ] Yes, it's a 3rd party (non-GitLab) link; `add link here`

Is there existing creative to consider or will we need to create our own?
_Creative elements, like images or videos, are required for sharing on organic social channels. [Check links using this social card validator](https://cards-dev.twitter.com/validator). If there is a card, we won't need to create additional assets._

* [ ]  There is an existing social card/opengraph attached to the link
* [ ]  Existing assets from the brand design team are here: `insert link to related issue or repo for creative`
* [ ]  I require new custom assets for social  (no card attached to link, no assets currently available)
* [ ]  I'm not sure what I require (the social team will review with you in the comments below) 

3. What is the overall utm_campaign? <!-- e.g., Campaign is *webcast123*, so the *utm_campaign=webcast123* * The social media team will use this tracking sheet to create URLs that are tied to social, and your campaign by using UTMs. Your UTM should be intuitive and should be as specific as the data you want to collect. UTM best practices: lowercase, not camelcase, and no spaces, .com or special characters (&*%$+#@, etc.) for new tags. If you are not sure, please speak with the marketer who is managing your event to ensure you are not interrupting the reporting structure they have in place. -->

**[add utm_campaign here]**

4. What objectives do you want users to follow? <!-- In most cases, every objective is not an appropriate course of action. Consider your event size, integrated marketing efforts, and other elements to be sure that social makes sense. -->
* [ ]  Pre-Event Sign-Ups/Registrations <!-- Available for GitLab owned and 3rd party events that are free or where there is a GitLab-specific registration code. -->
* [ ]  Awareness and/or Engagement during the event from users across social media, not at the event
* [ ]  Awareness and/or Engagement during the event from users who are at the event

5. Are there relevant hashtags or profiles to include in social copy? <!--Please only include strategic conversations that align with your particular event.-->

**[add hashtag(s) here]**

6. Are there handles/profiles you'd like the social media team to follow/monitor before/during your event? <!-- These specifics help the social team schedule engagement coverage during the event by monitoring user activity. -->

**[add links to handles/profiles here]**

#### Campaign Specifics

7. Are there existing assets to use on social or will you require custom designs?  <!-- links with cards, images, or videos -->
* [ ]  Existing Assets to use are located here: **[requester, insert link to related issue or repo for assets]**
* [ ]  I require custom assets for social
* [ ]  I'm not sure what I require (the social team will review with you in the comments below)

8. Would you like assistance with team enablement / advocacy for this event? 
<!-- The social team will create an issue outlining potential copy suggestions, links to assets, and shortlinks that are trackable, to aid team members in posting about the event on their own social channels. This is available for every event, can cover pre, during, and post event as necessary, and will require a larger window of time for completion. -->
* [ ]  No, I don't need assistance with enablement
* [ ]  Yes, I'd like assistance with enablement: **[social team to add advocacy issue here once created]**

9. Are you considering or will be adding paid social advertising for this campaign? <!-- Note that paid social advertising is managed by the digital marketing programs team and will require a separate issue -->
* [ ]  Yes, I'm requesting paid social advertising **[add link to paid social issue]**
* [ ]  No, I am not requesting paid social advertising
* [ ]  I'm not sure (the social team will help you determine the right path)

10. Anything else we should know or could be helpful to know?

**[add additional thoughts here]**

### 📝 Requester: To-Do's
* [ ]  I've filled out the above with the most information I have available
* [ ]  I've linked all of the related issues and epics to this issue
-----

## 📝 STEP 2: Social Team: To-Do's
<!-- Be sure there is enough detail with the info above. If an area is not pertinet (e.g. live coverage isn't necessary), please use a strikethrough <s> to <s> to cross out the text.-->  

* [ ]  Define the appropriate tier for social coverage: `INSERT TIER & EQUIVALENTS HERE` 
* [ ]  Offsite live coverage is needed - `INSERT SOCIAL TEAM MEMBER HANDLE` will manage/engage with users during `INSERT TIME WINDOW`
* [ ]  Asset Review: if asset links were provided, please review to be appropriate for social. If existing assets are not appropriate or if the requester asked for custom assets, please open a design issue, tag requester, and link as a related issue here.
* [ ]  Advocacy Request: if the requester asked for team enablement, please create an issue with the social-team-advocacy template, tag requester, and link as a related issue. If no request for advocacy, please check this box off as complete.

## ✍️ STEP 3: All: Drafts, suggestions, and schedule 
*Please work on these items in the comments below.*

## 🗓 STEP 4: Social Team: Scheduled posts

* [ ]  All posts that can be added to Sprout are scheduled
* [ ]  Comment on this issue with post and schedule details 
* [ ]  This campaign requires live community management coverage. `[insert social team member handle]` will be actively scheduled during `[window of time]`.

## 📭 Social Team: Once the posts available to add to Sprout are scheduled and live coverage is completed, you may close this issue.

<!-- POTENTIALLY NECESSARY DETAILS
Specifics for 
Twitter image (16:9 aspect ratio, min. 1200x675)
Facebook image (1200x630, less than 20% of image can have text) (test the Facebook Image to confirm: https://www.facebook.com/ads/tools/text_overlay)
LinkedIn image (1104x736)
Link Card (1200 x 630, uploaded to the opengraph asset folder)
--> 

/label ~Events ~"Social Media" ~"Corp Comms"~"Corporate Marketing" ~"mktg-status::plan"

/assign @social

/milestone %"Organic Social: Triage"
