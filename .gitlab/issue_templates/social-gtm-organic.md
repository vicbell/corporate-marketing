## Organic Social Request for GTM-aligned Content
_Each piece of content should get its own issue. These issues are intended to be attached to an `Organic Social Child-Epic for each GTM campaign. Other questions that are campaign-wide get answered there._

### 📬 STEP 1: For Requester from Campaign Team
##### 1.  What is your request all about?

`add content-specific/unique details here. consider linking to an epic/issue, a Google doc, or other items that may be necessary to understand before fulfilling the request. If the request has a different objective, audience, or other parameters (UTM), than the campaign child epic, describe those differences here. You do **not** need to duplicate information included in the organic social child-epic for this GTM campaign.`

 What user journey objectives are we trying to accomplish? (Pick 2 only)
* [ ]  The most people should know the information (objective = impressions)
* [ ]  I want people to respond, share info with others, and/or take action on-social channels (objective = engagements)
* [ ]  I want people to register, read, or continue off-social media channels (objective = clicks to site)

---

##### 2.  🗓 Pertinent Dates
- [ ] This is an event

Content launch date (For events, this is the day it happens): `x/xx/xx` 

Select one of the following:

- [ ] Promotion needs to be started by: `x/xx/xx` ➡️ _add as the due date to the right_
- [ ] Promote when calendar space is available

---

#####  3.  🔗 Add appropriate link(s) for user journey; Consider required creative
- [ ] None, this is a social only/first ask
- [ ] Yes, it's a GitLab owned link; `add link here`
- [ ] Yes, it's a 3rd party (non-GitLab) link; `add link here`

Is there existing creative to consider or will we need to create our own?
_Creative elements, like images or videos, are required for sharing on organic social channels. [Check links using this social card validator](https://cards-dev.twitter.com/validator). If there is a card, we won't need to create additional assets._

* [ ]  There is an existing social card/opengraph attached to the link
* [ ]  Existing assets from the brand design team are here: `insert link to related issue or repo for creative`
* [ ]  I require new custom assets for social  (no card attached to link, no assets currently available)
* [ ]  I'm not sure what I require (the social team will review with you in the comments below) 

---

##### 4. 💰 Are you considering paid social advertising for this content?

* [ ]  Yes, I'm requesting paid social advertising `add paid social request as a related issue`
* [ ]  No, I am not requesting paid social advertising

---

### 🗓 STEP 2: Social Team: Schedule posts, notify campaign manager in comments, close issue.

/label ~"Social Media" ~"Corp Comms" ~"Corporate Marketing" ~"mktg-status::plan" ~"usecase-gtm" ~"Stage::Awareness"

/assign @social

/milestone %"Organic Social: Triage"
