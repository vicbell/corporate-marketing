
## Description of Opportunity

<!--- Whatever details you have at the moment. -->

## Counterpart section

Please add the `de-sales`, `de-alliances` or `de-engineering` label as relevant to your department.

[ ] Alliances
[ ] Engineering/ Product
[ ] Sales

## Type of commitment required

[ ] Webcast
[ ] Blog Post
[ ] Video
[ ] Technology Evaluation
[ ] Talk submission [CFP Issue Link]

## Non-Disclosure Agreement
Is this oppurtunity under an NDA, you can make this issue confidential. If a blog post is a deliverable for this oppotunity, please follow the directions for [creating MRs for confidential issues](https://about.gitlab.com/handbook/marketing/blog/#creating-mrs-for-confidential-issues).

## Resources

Any links, documents or resource relevant to the oppurtunity.


/label ~"dev-evangelism" 
