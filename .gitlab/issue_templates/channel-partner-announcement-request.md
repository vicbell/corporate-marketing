### Request for review and approval of channel/technology partner-led announcement.

<!-- Requester please fill in all relevant sections. Some details may not be applicable. Please see the handbook for more information 
on requesting an announcement: https://about.gitlab.com/handbook/marketing/product-marketing/analyst-relations/channel-marketing/#when-to-reach-out-to-channel-marketing-for-gtm-support. --> 

If a channel or technology partner is interested in taking the lead on making a formal announcement around joining the GitLab Partner Program and/or becoming a Certified Services Partner, please feel free to follow the instructions outlined in the Channel Marketing handbook section, "Partner needs support of their press release (PR) with a quote from GitLab," [here](https://about.gitlab.com/handbook/marketing/strategic-marketing/analyst-relations/channel-marketing/#partner-needs-support-of-their-press-release-pr-with-a-quote-from-gitlab). The below templates are a recommended guide for partners to customize based on their specific relationship with GitLab and area of expertise.
- [Joining the Partner Program (Open, Select, Tech Alliance) Press Release Template](https://docs.google.com/document/d/12B7wwjeaU9FJqk8nT2obXS08IAJz4LAdtajeM_JMQjs/edit) 
- [Becoming a Certified Services Partner (Professional, Managed, Training) Press Release Template](https://docs.google.com/document/d/1AKQeh7u64XXLgwOjhAIi1FcUW2N5-f0McUb1bTsd1aQ/edit) 

GitLab is happy to support the partner company’s announcement by providing a supporting quote from either Michelle Hodges (Channel Partners) or an Alliance manager (Technology Partners), quote attribution based on your partner track. 

Please follow the below steps:

#### To-Do's
##### Step 1: `Channel Account Manager` or `GitLab DRI for Partner` to send press release template to partner for completion (see above for template link in handbook). Once complete, send the below details and completed press release draft to @lrom and @cweaver1 via this issue template for GitLab internal reviews/approval.
* [ ] Partner press release template completed and link to document included below. 

##### Step 2: `PR Team Member` to route the partner press release through internal reviews and approval (add relevant approver names below).
* [ ] Christina Weaver/CorpComms-PR 
* [ ] Lisa Rom/Channel-Partner Marketing 
* [ ] Channel Account Manager or Alliances Team Member
* [ ] Michelle Hodges or Alliance Manager quoted

##### Step 3: `PR Team Member` to share approved press release draft including any suggested edits within 7-10 business days of request date to the partner company PR/marketing DRI and CC all GitLab DRIs on the email (channel marketing, CAM, etc). 
* [ ] Approved press release sent to partner for announcement coordination

##### Step 4: For `Requester` to complete after press release has been approved and timing has been confirmed.
###### Social Media
* [ ] If social amp is of interest, please open a new [social-general-request issue template in the Corporate Marketing Project](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/new?issuable_template=social-general-request) and link issues.
* [ ] If a partner is leading/participating in the announcement, *please encourage them to tag our company pages on every channel they are pushing the news.* [Links to all our channels are here](https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/#social-channels). This makes engagement and amplification of their content easier, and helps GitLab social channels grow.

------

### Details

#### Name of the channel/technology partner that would like to announce joining the GitLab Partner Program?: 

[add details here]

#### Has the partner contract been finalized and signed?

- [ ] Yes
- [ ] No [If no, please note that the partner agreement/contract must be signed before making a formal announcement.]

#### Link to the completed partner press release draft.

[Included Google Doc link to the press release draft here.]

#### What is the date that the partner wants to share the announcement, ideally?

[add details here]

#### Is this proposed announcement date tied to a specific industry/partner event? If so, what is the date of the event?

[add details here]

#### Who is the GitLab DRI for the announcement? 

[add details here]

#### Any additional GitLab SMEs who need to be a part of the review/approval process?

[add details here]

#### Who is the PR/marketing DRI at the partner company? Please share name and contact info for coordination on approvals and comms activities.

[Include name(s) and email(s)]

####  Link related issues and epics
   - [ ] Please link to all related issues and the epic

------

/label ~"corporate marketing" ~"channel marketing" ~"corp comms" ~"PR" ~"announcement" ~"mktg-status::plan" ~"channel" ~"Partner Mktg:: New Request"

/cc @cweaver1 @nwoods1 @KatieHWPR @lrom

/confidential
