---
`**For Event Planners complete once event contract signed- this is for our own internal tracking purposes**`

### :construction_site: Pre- Event Checklist (not every item below will be relevant to every show. Delete any items not necessary)
* [ ] Event added to Events Cal and Events page
* [ ] Artwork sent- this is for posting on the event page and on site signage
* [ ] Company description (use [Value Prop Messaging](https://about.gitlab.com/handbook/marketing/product-marketing/messaging/#gitlab-value-proposition). Ping PMM if you need something longer or specific)
* [ ] Create and share planning spreadsheet (included travel, booth duty, meetings...) from [planning sheet template](https://docs.google.com/spreadsheets/d/1rXzp1bLoiI-IJ86Jbe39Kif2iujQoWVNXsbxQisFwUY/edit#gid=1085564346)
* [ ] Slack channel created and attendees invited (Naming conventuon for channel- use event name, location, and year. link to Epic in channel description)
* [ ] Staffing selected and Tickets allocated   
* [ ] Decide on if we will do swag and any other printed materials (see swag selcetion below if you are shipping swag)- start swag issue with swag template
* [ ] Attendee directory from organizers (it is not common to get the list of attendees)
* [ ] Press list sent to PR team- create PR issue if needed
* [ ] Social media copy written and scheduled - there is an issue template for this (give them as much notice as possible)
* [ ] Flights/ transport/ lodging booked- added to spreadsheet- use travel template
* [ ] Final prep meeting scheduled (end of week before the show begins. Include everyone attending, planning and person from PMM who will do demo training)
* [ ] Event post mortem scheduled (1-2 weeks post event)
* [ ] Post event feedback survey created- to be shared post event in meta issue.
* [ ] Event recap (week after) & Feedback collected. [Recap Template](https://docs.google.com/document/d/1qHFOd0TU52Eq-fnYUyMU7E3-FninPciMH9kA7F5yofM/edit)

## :performing_arts: Booth/Theatre
* [ ]  Booth Design/ Virtual Booth Issue created- design needs at least 2 weeks notice to design (see handbook]([Epic](https://about.gitlab.com/handbook/marketing/corporate-marketing/#corporate-event-execution-process)
* [ ]  Positioning and Messaging confirmed
* [ ]  [AV ordered- monitors] `(!add conf number)`
* [ ]  [Electric ordered & conf number] `(!add conf number)`
* [ ]  [Screen and HDMI cable ordered] `(!add conf number)`
* [ ]  [Furnishing ordered (Countertop, Stools, Coffee table)] `(!add conf number)`
* [ ]  Booth staff scheduled- add link to schedule in staffing issue
* [ ]  Booth slide deck created/ decided on
* [ ]  Share booth deck on slack channel- best to share published, shortened url
* [ ]  Click through demo setup (iPad)
* [ ]  Power if not incuded in package or need more
* [ ]  Final mock up approved
* [ ]  Booth Artwork issue created and artwork submitted - link to issue

### :iphone: Lead scanning
Badge scanners- get 1 for around every 1000 people at the show, but do not exceed amount of people we have staffing the booth at one time. 
* [ ]  Lead Scanner / App ordered: `qty`
* [ ]  [Lead Scanner conf number] `(!add conf number)`
* [ ]  Lead license assigned: @username

Assign to DRI! 
Add due date as event Date.

~Events ~"Corporate Event" ~"Corporate Marketing" ~"mktg-status:WIP"
