<!-- Hey, Thank you for taking the time to create an issue for your talk, this helps us coordinate submissions and have a central place for submissions to a CFP -->

<!-- Please use the format below to add an Issue title. -->

<!-- CFP: <Event Name>, <Event Dates> - Due: <CFP Due Date> -->

## Event Details

* `Name of Event`: 
* `Event Website`:
* `Event Dates`: 
* `Location`: 
* `CFP Closes`: 
* `CFP Notification Date`:  
* `CFP Submission Link`: 
* `GitLab Event Issue/Epic`: <!-- Link to Issue if available -->

### Open Source / Education Conference CFPs

If the conference is organized by an open source (OSS) or educational (EDU) organization, or if OSS or EDU topics may be of interest at the conference, please check the appropriate checkbox to add the relevant labels

* [ ] Education Conference ~CFP-EDU
* [ ] Opensource Software or Organization ~CFP-OSS

## CFP Description

<!-- Add details description, including any other additional information that will be useful for other team members, who might also be interested in submitting. -->

### CFP Submission Guidelines

<!-- You can copy and paste the Guidelines from the organizer's website or add the link here-->

## Co-ordination

### Drafts

Please create a folder in the shared CFP [Google Drive folder](https://drive.google.com/drive/folders/1IyHNvb6IT_rTieoFzN9d2VV63Nj57rVT?usp=sharing) for the event (if one doesn't exist already) and another folder for the year (inside the event folder). Kindly create submission documents for each submission in the current year folder of the event and add the link to it along with the submission details below. You can copy+paste an existing one and edit.

<!-- Repeat for each Draft/Submission! -->

####  Talk Title
 - Speaker: 
 - [Resource Folder]()
 - [ ] Draft ready for review
 - [ ] Peer-Reviewed
 - [ ] Submitted
 - [ ] Accepted
 - [ ] Added to [CFP Library](https://docs.google.com/spreadsheets/d/1KX8uf-4Ov8ybztJibQlGr9HvgH9VobpA8Nv5ecny1N4/edit#gid=0)
 - [ ] Talk Delivered: [Slides]() <!-- Add link to Talk slides and where possible, add the links to the CFP doc. -->





## DE Checklist

* [ ] Internally Promoted
* [ ] Speaker Sourcing
* [ ] [Community Reachout](https://gitter.im/gitlab/heroes?at=5fa2a6fa8a236947ba85474a) 
* [ ] Talks Submitted (`0`)
* [ ] Talks Accepted (`0/0`)


FYI:  @johncoghlan @dnsmichi @brendan @abuango

/assign me @abuango

<!-- Please leave the label below on this issue -->

/label ~Events ~Speaker ~SPIFF ~"mktg-status::plan" ~"dev-evangelism" ~"CFP" ~"CFP::Open"

<!-- Please leave the label below on this issue -->
