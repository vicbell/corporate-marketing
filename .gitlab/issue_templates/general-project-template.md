<!-- Below is a standard project issue template. This is useful for assembling high-level details about a project or opportunity, with a few emojis tossed in for visual pizzazz. You can remove headers and sections that are not relevant, or add headers you need. For more on how to make beautiful templates, check out GitLab's Markdown Guide (https://about.gitlab.com/handbook/engineering/ux/technical-writing/markdown-guide/). To add an emoji, begin by typing `:` and the title of the emoji. A list of emoji markup is here (https://gist.github.com/rxaviers/7360908). Remember to always add Related Issues and Epics after you've created your issue so folks have context on what other issues connect to this work. Keep being awesome! -->



## :pencil: Epic link

<!-- If this issue is connected to an Epic, link the above subhead for easy discover. If not, you can remove.  -->

## :speaking_head_in_silhouette: Social request issue

<!-- If this issue is connected to a related social request issue, link the above subhead for easy discover. If not, you can remove.  -->

## :rolled_up_newspaper: PR request issue

<!-- If this issue is connected to a related PR request issue, link the above subhead for easy discover. If not, you can remove.  -->

## :star: Finance issue

<!-- If this issue is connected to a related finance issue, link the above subhead for easy discover. If not, you can remove. -->

## :bulb: Background/purpose

<!-- Give a brief 1-2 sentence overview of why this issue exists. Treat this as a snapshot to understanding this work.  -->

## :map: Details and reach

<!-- Include details on why you're completing this work, how it impacts the business, and the potential reach if applicable. Include key dates as well.  -->

## :dart: Goals and key messages

<!-- Add context on the goals for this work. Add key messages if applicable. This helps those who may be unfamiliar with your functional group have a better understanding of the work.  -->

## :notebook_with_decorative_cover: Event brief

<!-- Link to this subhead if you've completed an executive brief to submit for an event.  -->

## :writing_hand: Event, panel, or keynote details

<!-- Remove this section if no event, keynote, or panel is involved.  -->

* **Name of event**: `INSERT HERE`
* **Event website**: `INSERT HERE`
* **Location**:  `INSERT HERE`
* **Date**: `INSERT HERE`
* **Time**: `INSERT HERE`
* **Description**: `INSERT HERE`
* **Format**: `INSERT HERE`
* **Travel/scheduling details**: `INSERT HERE`

## :stopwatch: Due dates, DRI(s) and next steps/to-dos :ballot_box_with_check:

<!-- Use this section to indicate tasks and DRIs for each task.  -->










<!-- Please leave the label below on this issue -->
/label ~"Corporate Marketing" ~"mktg-status::plan" 